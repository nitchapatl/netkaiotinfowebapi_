﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NetkaIoTInfoWebApi.Models;
using NetkaIoTInfoWebApi.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NetkaIoTInfoWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class NetkaInfoGatewayDetailController : Controller
    {

        private readonly NetkaInfoGatewayDetailRepository _NetkaInfoGatewayDetailRepository;
        public NetkaInfoGatewayDetailController()
        {
            _NetkaInfoGatewayDetailRepository = new NetkaInfoGatewayDetailRepository();
        }


        /*// GET: api/NetkaInfoGatewayDetail/all
        [HttpGet("all")]
        public IEnumerable<NetkaInfoGatewayDetailViewModel> GetNetkaInfoGatewayDetailAll()
        {
            return _NetkaInfoGatewayDetailRepository.GetNetkaInfoGatewayDetailAll();
        }*/

        // GET: api/NetkaInfoGatewayDetail/number
        [HttpGet("number")]
        public long GetNetkaInfoGatewayDetailNumber()
        {
            return _NetkaInfoGatewayDetailRepository.GetNetkaInfoGatewayDetailNumber();
        }

        /*// GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }*/
    }
}
