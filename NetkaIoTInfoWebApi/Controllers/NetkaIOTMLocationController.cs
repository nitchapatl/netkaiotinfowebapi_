﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NetkaIoTInfoWebApi.Models;
using NetkaIoTInfoWebApi.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NetkaIoTInfoWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class NetkaIOTMLocationController : Controller
    {

        private readonly NetkaIOTMLocationRepository _NetkaIOTMLocationRepository;
        public NetkaIOTMLocationController()
        {
            _NetkaIOTMLocationRepository = new NetkaIOTMLocationRepository();
        }

        // GET: api/NetkaIOTMLocation
        [HttpGet]
        public IEnumerable<NetkaIOTMLocationViewModel> GetNetkaIOTMLocation()
        {
            return _NetkaIOTMLocationRepository.GetNetkaIOTMLocation();
        }

        // POST: api/NetkaIOTMLocation
        [HttpPost]
        public void AddNetkaIOTMLocation([FromBody]NetkaIOTMLocationViewModel model)
        {
            var mlocation = new NetkaIOTMLocationViewModel
            {
                Id = Guid.NewGuid(),
                Location = model.Location,
                Country = model.Country
            };
            _NetkaIOTMLocationRepository.AddNetkaIOTMLocation(mlocation);
        }

        /*// GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }*/
    }
}
