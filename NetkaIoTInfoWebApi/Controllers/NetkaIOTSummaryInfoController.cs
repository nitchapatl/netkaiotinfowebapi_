﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NetkaIoTInfoWebApi.Models;
using NetkaIoTInfoWebApi.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NetkaIoTInfoWebApi.Controllers
{
    [Route("api/[controller]")]
    public class NetkaIOTSummaryInfoController : Controller
    {

        private readonly NetkaIOTSummaryInfoRepository _NetkaIOTSummaryInfoRepository;
        public NetkaIOTSummaryInfoController()
        {
            _NetkaIOTSummaryInfoRepository = new NetkaIOTSummaryInfoRepository();
        }

        // GET: api/NetkaIOTSummaryInfo/netkaiotlocation
        [HttpGet("netkaiotlocation")]
        public IEnumerable<NetkaIOTSummaryInfoViewModel> GetNetkaIOTSummaryInfoNetkaIOTLocation()
        {
            return _NetkaIOTSummaryInfoRepository.GetNetkaIOTSummaryInfoNetkaIOTLocation();
        }

        // GET: api/NetkaIOTSummaryInfo/netkainfo
        [HttpGet("netkainfo")]
        public IEnumerable<NetkaIOTSummaryInfoViewModel> GetNetkaIOTSummaryInfoNetkaInfo()
        {
            return _NetkaIOTSummaryInfoRepository.GetNetkaIOTSummaryInfoNetkaInfo();
        }

        // GET: api/NetkaIOTSummaryInfo/netkainfogateway
        [HttpGet("netkainfogateway")]
        public IEnumerable<NetkaIOTSummaryInfoViewModel> GetNetkaIOTSummaryInfoNetkaInfoGateway()
        {
            return _NetkaIOTSummaryInfoRepository.GetNetkaIOTSummaryInfoNetkaInfoGateway();
        }

        // GET: api/NetkaIOTSummaryInfo/netkainfogatewaydetail
        [HttpGet("netkainfogatewaydetail")]
        public IEnumerable<NetkaIOTSummaryInfoViewModel> GetNetkaIOTSummaryInfoNetkaInfoGatewayDetail()
        {
            return _NetkaIOTSummaryInfoRepository.GetNetkaIOTSummaryInfoNetkaInfoGatewayDetail();
        }

        /*// GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }*/
    }
}
