﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nest;
using NetkaIoTInfoWebApi.Connector;
using NetkaIoTInfoWebApi.Models;

namespace NetkaIoTInfoWebApi.Services
{
    public class NetkaIOTLocationRepository
    {

        private readonly ConnectionToEs _connectionToEs;
        public NetkaIOTLocationRepository()
        {
            _connectionToEs = new ConnectionToEs();
        }

        public IEnumerable<NetkaIOTLocationViewModel> GetNetkaIOTLocationAll()
        {
            List<NetkaIOTLocationViewModel> ls_result = new List<NetkaIOTLocationViewModel>();
            var scanResults = _connectionToEs.EsClient().Search<NetkaIOTLocationViewModel>(s => s
                                .Index("netka_iot_location")
                                .From(0)
                                .Size(2000)
                                .Query(q => q.MatchAll())
                                //.SearchType(Elasticsearch.Net.SearchType.QueryThenFetch)
                                .Scroll("5m")
                                );

            var results = _connectionToEs.EsClient().Scroll<NetkaIOTLocationViewModel>("10m", scanResults.ScrollId);
            while (results.Documents.Any())
            {
                
                foreach (var doc in results.Documents)
                {
                    ls_result.Add(doc);
                }

                results = _connectionToEs.EsClient().Scroll<NetkaIOTLocationViewModel>("10m", results.ScrollId);
            }

            return ls_result.AsEnumerable();
        }

        public long GetNetkaIOTLocationNumber()
        {
            var result = _connectionToEs.EsClient().Count<NetkaIOTLocationViewModel>(s => s
                                .Index("netka_iot_location")
                                .Query(q => q.MatchAll())
                                );

            return result.Count;
        }
    }
}
