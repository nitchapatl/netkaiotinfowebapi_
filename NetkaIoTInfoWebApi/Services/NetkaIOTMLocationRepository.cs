﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NetkaIoTInfoWebApi.Connector;
using NetkaIoTInfoWebApi.Models;

namespace NetkaIoTInfoWebApi.Services
{
    public class NetkaIOTMLocationRepository
    {
        private readonly ConnectionToEs _connectionToEs;
        public NetkaIOTMLocationRepository()
        {
            _connectionToEs = new ConnectionToEs();
        }

        public IEnumerable<NetkaIOTMLocationViewModel> GetNetkaIOTMLocation()
        {
            var response = _connectionToEs.EsClient().Search<NetkaIOTMLocationViewModel>(s => s
                                    .Index("netka_iot_mlocation")
                                    .Query(q => q.MatchAll())
                                    .From(0)
                                    .Size(1000)
                                );

            return response.Documents.AsEnumerable();
        }



        public void AddNetkaIOTMLocation(NetkaIOTMLocationViewModel model)
        {

            if (!(_connectionToEs.EsClient().Indices.Exists("netka_iot_mlocation").Exists))
            {
                _connectionToEs.EsClient().Indices.Create("netka_iot_mlocation",
                    index => index.Map<NetkaIOTMLocationViewModel>(
                        x => x.AutoMap()
                    ));
            }

            _connectionToEs.EsClient().Index(model, i => i
                .Index("netka_iot_mlocation")
                .Id(model.Id.ToString())
                .Refresh(Elasticsearch.Net.Refresh.True));
        }
    }
}
