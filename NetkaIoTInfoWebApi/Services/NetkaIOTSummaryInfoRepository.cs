﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nest;
using NetkaIoTInfoWebApi.Connector;
using NetkaIoTInfoWebApi.Models;

namespace NetkaIoTInfoWebApi.Services
{
    public class NetkaIOTSummaryInfoRepository
    {

        private readonly ConnectionToEs _connectionToEs;
        public NetkaIOTSummaryInfoRepository()
        {
            _connectionToEs = new ConnectionToEs();
        }

        public IEnumerable<NetkaIOTSummaryInfoViewModel> GetNetkaIOTSummaryInfoNetkaIOTLocation()
        {
            List<NetkaIOTSummaryInfoViewModel> ls_IoTSummaryInfo = new List<NetkaIOTSummaryInfoViewModel>();

            // 1 receive location 
            var response = _connectionToEs.EsClient().Search<NetkaIOTMLocationViewModel>(s => s
                                    .Index("netka_iot_mlocation")
                                    .Query(q => q.MatchAll())
                                    .From(0)
                                    .Size(1000)
                                );
            foreach (var hits in response.Hits)
            {
                NetkaIOTSummaryInfoViewModel IOTSummaryInfo = new NetkaIOTSummaryInfoViewModel();

                IOTSummaryInfo.Key = hits.Source.Location.ToString();

                // 2 receive totalgateway
                var result = _connectionToEs.EsClient().Search<NetkaIOTLocationViewModel>(s => s
                                .Index("netka_iot_location")
                                .Query(q => q.Match(m => m.Field("location").Query(IOTSummaryInfo.Key)))
                                .Aggregations(a => a
                                    .Cardinality("totalgateway", t => t
                                        .Field(f => f.Macaddress.Suffix("keyword"))
                                        )
                                    )
                                );

                var totalgateway = result.Aggregations.Cardinality("totalgateway").Value;

                IOTSummaryInfo.Totalgateway = Convert.ToInt32(totalgateway);

                ls_IoTSummaryInfo.Add(IOTSummaryInfo);
            }


           return ls_IoTSummaryInfo.AsEnumerable();
        }

        public int GetNetkaIOTSummaryInfoNetkaIOTLocationNumber()
        {
            var result = _connectionToEs.EsClient().Search<NetkaIOTLocationViewModel>(s => s
                                .Index("netka_iot_location")
                                .Query(q => q.Match(m => m.Field("location").Query("Bangkok")))
                                .Aggregations(a => a
                                    .Cardinality("totalgateway", t => t
                                        .Field(f => f.Macaddress.Suffix("keyword"))
                                        )
                                    )
                                );

            int agg = Convert.ToInt32(result.Aggregations.Cardinality("totalgateway").Value);

            return agg;
        }

        public IEnumerable<NetkaIOTSummaryInfoViewModel> GetNetkaIOTSummaryInfoNetkaInfo()
        {
            List<NetkaIOTSummaryInfoViewModel> ls_IoTSummaryInfo = new List<NetkaIOTSummaryInfoViewModel>();

            // 1 receive location 
            var response = _connectionToEs.EsClient().Search<NetkaIOTMLocationViewModel>(s => s
                                    .Index("netka_iot_mlocation")
                                    .Query(q => q.MatchAll())
                                    .From(0)
                                    .Size(1000)
                                );
            foreach (var hits in response.Hits)
            {
                NetkaIOTSummaryInfoViewModel IOTSummaryInfo = new NetkaIOTSummaryInfoViewModel();

                IOTSummaryInfo.Key = hits.Source.Location.ToString();

                // 2 receive totalgateway
                var result = _connectionToEs.EsClient().Search<NetkaIOTLocationViewModel>(s => s
                                .Index("netka_info")
                                .Query(q => q.Match(m => m.Field("location").Query(IOTSummaryInfo.Key)))
                                .Aggregations(a => a
                                    .Cardinality("totalgateway", t => t
                                        .Field(f => f.Macaddress.Suffix("keyword"))
                                        )
                                    )
                                );

                var totalgateway = result.Aggregations.Cardinality("totalgateway").Value;

                IOTSummaryInfo.Totalgateway = Convert.ToInt32(totalgateway);

                ls_IoTSummaryInfo.Add(IOTSummaryInfo);
            }


            return ls_IoTSummaryInfo.AsEnumerable();
        }

        public IEnumerable<NetkaIOTSummaryInfoViewModel> GetNetkaIOTSummaryInfoNetkaInfoGateway()
        {
            List<NetkaIOTSummaryInfoViewModel> ls_IoTSummaryInfo = new List<NetkaIOTSummaryInfoViewModel>();

            // 1 receive location 
            var response = _connectionToEs.EsClient().Search<NetkaIOTMLocationViewModel>(s => s
                                    .Index("netka_iot_mlocation")
                                    .Query(q => q.MatchAll())
                                    .From(0)
                                    .Size(1000)
                                );
            foreach (var hits in response.Hits)
            {
                NetkaIOTSummaryInfoViewModel IOTSummaryInfo = new NetkaIOTSummaryInfoViewModel();

                IOTSummaryInfo.Key = hits.Source.Location.ToString();

                // 2 receive totalgateway
                var result = _connectionToEs.EsClient().Search<NetkaIOTLocationViewModel>(s => s
                                .Index("netka_info_gateway")
                                .Query(q => q.Match(m => m.Field("location").Query(IOTSummaryInfo.Key)))
                                .Aggregations(a => a
                                    .Cardinality("totalgateway", t => t
                                        .Field(f => f.Macaddress.Suffix("keyword"))
                                        )
                                    )
                                );

                var totalgateway = result.Aggregations.Cardinality("totalgateway").Value;

                IOTSummaryInfo.Totalgateway = Convert.ToInt32(totalgateway);

                ls_IoTSummaryInfo.Add(IOTSummaryInfo);
            }


            return ls_IoTSummaryInfo.AsEnumerable();
        }

        public IEnumerable<NetkaIOTSummaryInfoViewModel> GetNetkaIOTSummaryInfoNetkaInfoGatewayDetail()
        {
            List<NetkaIOTSummaryInfoViewModel> ls_IoTSummaryInfo = new List<NetkaIOTSummaryInfoViewModel>();

            // 1 receive location 
            var response = _connectionToEs.EsClient().Search<NetkaIOTMLocationViewModel>(s => s
                                    .Index("netka_iot_mlocation")
                                    .Query(q => q.MatchAll())
                                    .From(0)
                                    .Size(1000)
                                );
            foreach (var hits in response.Hits)
            {
                NetkaIOTSummaryInfoViewModel IOTSummaryInfo = new NetkaIOTSummaryInfoViewModel();

                IOTSummaryInfo.Key = hits.Source.Location.ToString();

                // 2 receive totalgateway
                var result = _connectionToEs.EsClient().Search<NetkaIOTLocationViewModel>(s => s
                                .Index("netka_info_gateway_detail")
                                .Query(q => q.Match(m => m.Field("location").Query(IOTSummaryInfo.Key)))
                                .Aggregations(a => a
                                    .Cardinality("totalgateway", t => t
                                        .Field(f => f.Macaddress.Suffix("keyword"))
                                        )
                                    )
                                );

                var totalgateway = result.Aggregations.Cardinality("totalgateway").Value;

                IOTSummaryInfo.Totalgateway = Convert.ToInt32(totalgateway);

                ls_IoTSummaryInfo.Add(IOTSummaryInfo);
            }


            return ls_IoTSummaryInfo.AsEnumerable();
        }
    }
}
