﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nest;
using NetkaIoTInfoWebApi.Connector;
using NetkaIoTInfoWebApi.Models;

namespace NetkaIoTInfoWebApi.Services
{
    public class NetkaInfoGatewayRepository
    {

        private readonly ConnectionToEs _connectionToEs;
        public NetkaInfoGatewayRepository()
        {
            _connectionToEs = new ConnectionToEs();
        }

        public IEnumerable<NetkaInfoGatewayViewModel> GetNetkaInfoGatewayAll()
        {
            List<NetkaInfoGatewayViewModel> ls_result = new List<NetkaInfoGatewayViewModel>();
            var scanResults = _connectionToEs.EsClient().Search<NetkaInfoGatewayViewModel>(s => s
                                .Index("netka_info_gateway")
                                .From(0)
                                .Size(2000)
                                .Query(q => q.MatchAll())
                                //.SearchType(Elasticsearch.Net.SearchType.QueryThenFetch)
                                .Scroll("5m")
                                );

            var results = _connectionToEs.EsClient().Scroll<NetkaInfoGatewayViewModel>("10m", scanResults.ScrollId);
            while (results.Documents.Any())
            {

                foreach (var doc in results.Documents)
                {
                    ls_result.Add(doc);
                }

                results = _connectionToEs.EsClient().Scroll<NetkaInfoGatewayViewModel>("10m", results.ScrollId);
            }

            return ls_result.AsEnumerable();
        }

        public long GetNetkaInfoGatewayNumber()
        {
            var result = _connectionToEs.EsClient().Count<NetkaInfoGatewayViewModel>(s => s
                                .Index("netka_info_gateway")
                                .Query(q => q.MatchAll())
                                );

            return result.Count;
        }
    }
}
