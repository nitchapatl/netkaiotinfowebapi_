﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nest;
using NetkaIoTInfoWebApi.Connector;
using NetkaIoTInfoWebApi.Models;

namespace NetkaIoTInfoWebApi.Services
{
    public class NetkaInfoRepository
    {

        private readonly ConnectionToEs _connectionToEs;
        public NetkaInfoRepository()
        {
            _connectionToEs = new ConnectionToEs();
        }

        public IEnumerable<NetkaInfoViewModel> GetNetkaInfoAll()
        {
            List<NetkaInfoViewModel> ls_result = new List<NetkaInfoViewModel>();
            var scanResults = _connectionToEs.EsClient().Search<NetkaInfoViewModel>(s => s
                                .Index("netka_info")
                                .From(0)
                                .Size(2000)
                                .Query(q => q.MatchAll())
                                //.SearchType(Elasticsearch.Net.SearchType.QueryThenFetch)
                                .Scroll("5m")
                                );

            var results = _connectionToEs.EsClient().Scroll<NetkaInfoViewModel>("10m", scanResults.ScrollId);
            while (results.Documents.Any())
            {

                foreach (var doc in results.Documents)
                {
                    ls_result.Add(doc);
                }

                results = _connectionToEs.EsClient().Scroll<NetkaInfoViewModel>("10m", results.ScrollId);
            }

            return ls_result.AsEnumerable();
        }

        public long GetNetkaInfoNumber()
        {
            var result = _connectionToEs.EsClient().Count<NetkaInfoViewModel>(s => s
                                .Index("netka_info")
                                .Query(q => q.MatchAll())
                                );

            return result.Count;
        }
    }
}
