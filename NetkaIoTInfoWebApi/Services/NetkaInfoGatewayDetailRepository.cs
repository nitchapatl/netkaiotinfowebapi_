﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nest;
using NetkaIoTInfoWebApi.Connector;
using NetkaIoTInfoWebApi.Models;

namespace NetkaIoTInfoWebApi.Services
{
    public class NetkaInfoGatewayDetailRepository
    {

        private readonly ConnectionToEs _connectionToEs;
        public NetkaInfoGatewayDetailRepository()
        {
            _connectionToEs = new ConnectionToEs();
        }

        public IEnumerable<NetkaInfoGatewayDetailViewModel> GetNetkaInfoGatewayDetailAll()
        {
            List<NetkaInfoGatewayDetailViewModel> ls_result = new List<NetkaInfoGatewayDetailViewModel>();
            var scanResults = _connectionToEs.EsClient().Search<NetkaInfoGatewayDetailViewModel>(s => s
                                .Index("netka_info_gateway_detail")
                                .From(0)
                                .Size(2000)
                                .Query(q => q.MatchAll())
                                //.SearchType(Elasticsearch.Net.SearchType.QueryThenFetch)
                                .Scroll("5m")
                                );

            var results = _connectionToEs.EsClient().Scroll<NetkaInfoGatewayDetailViewModel>("10m", scanResults.ScrollId);
            while (results.Documents.Any())
            {

                foreach (var doc in results.Documents)
                {
                    ls_result.Add(doc);
                }

                results = _connectionToEs.EsClient().Scroll<NetkaInfoGatewayDetailViewModel>("10m", results.ScrollId);
            }

            return ls_result.AsEnumerable();
        }

        public long GetNetkaInfoGatewayDetailNumber()
        {
            var result = _connectionToEs.EsClient().Count<NetkaInfoGatewayDetailViewModel>(s => s
                                .Index("netka_info_gateway_detail")
                                .Query(q => q.MatchAll())
                                );

            return result.Count;
        }
    }
}
