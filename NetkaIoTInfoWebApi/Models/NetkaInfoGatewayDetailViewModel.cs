﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nest;

namespace NetkaIoTInfoWebApi.Models
{
    public class NetkaInfoGatewayDetailViewModel
    {
        [ElasticsearchType(RelationName = "netka_info_gateway_detail")]
        public class NetkaInfoGatewayViewModel
        {
            [PropertyName("datetime")]
            public DateTime Datetime { get; set; }

            [PropertyName("gateway")]
            public string Gateway { get; set; }

            [PropertyName("id")]
            public double Id { get; set; }

            [PropertyName("ipAddress")]
            public string Ipaddress { get; set; }

            [PropertyName("location")]
            public string Location { get; set; }

            [PropertyName("macAddress")]
            public string Macaddress { get; set; }

            [PropertyName("numberOfSensor")]
            public int NumberOfSensor { get; set; }

            [PropertyName("sensor")]
            public string Sensor { get; set; }

            [PropertyName("value")]
            public string Value { get; set; }
        }
    }
}
