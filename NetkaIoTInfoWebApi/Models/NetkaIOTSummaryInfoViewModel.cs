﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetkaIoTInfoWebApi.Models
{
    public class NetkaIOTSummaryInfoViewModel
    {
        public string Key { get; set; }

        public int Totalgateway { get; set; }
    }
}
