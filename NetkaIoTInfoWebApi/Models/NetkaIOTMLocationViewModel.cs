﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nest;

namespace NetkaIoTInfoWebApi.Models
{

    [ElasticsearchType(RelationName = "netka_iot_mlocation")]
    public class NetkaIOTMLocationViewModel
    {
        [PropertyName("id")]
        public Guid Id { get; set; }

        [PropertyName("location")]
        public string Location { get; set; }

        [PropertyName("country")]
        public string Country { get; set; }
    }
}
