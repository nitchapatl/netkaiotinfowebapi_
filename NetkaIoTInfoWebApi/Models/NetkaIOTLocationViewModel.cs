﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Nest;

namespace NetkaIoTInfoWebApi.Models
{

    [ElasticsearchType(RelationName = "netka_iot_location")]
    public class NetkaIOTLocationViewModel
    {
        [PropertyName("datetime")]
        public DateTime Datetime { get; set; }

        [PropertyName("gateway")]
        public string Gateway { get; set; }

        [PropertyName("id")]
        public double Id { get; set; }

        [PropertyName("ipAddress")]
        public string Ipaddress { get; set; }

        [PropertyName("location")]
        public string Location { get; set; }

        [PropertyName("macAddress")]
        public string Macaddress { get; set; }

        [PropertyName("sensor")]
        public string Sensor { get; set; }

        [PropertyName("value")]
        public string Value { get; set;  }

    }
}
